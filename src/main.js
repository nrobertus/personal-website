import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueAnimateOnScroll from 'vue3-animate-onscroll';
import PerfectScrollbar from 'vue3-perfect-scrollbar'
import 'vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css'
import Popper from "vue3-popper";


import './assets/main.css'

const app = createApp(App)
app.use(VueAnimateOnScroll);
app.use(PerfectScrollbar)
app.use(router)
app.component("Popper", Popper);

app.mount('#app')
